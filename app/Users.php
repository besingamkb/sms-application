<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
	/**
	 * set users as the table
	 * @var string
	 */
    protected $table = "users";
}

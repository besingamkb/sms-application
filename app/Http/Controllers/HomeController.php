<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Session;
use App\Http\SmsGateway\SmsGateway;

class HomeController extends Controller
{
	/**
	 * index action
	 * @return [view] [index view]
	 */
    public function index() 
    {
    	if (!Session::get('is_logged_in')) {
    		return redirect('user/login');
    	}

    	// $SmsGateway = new SmsGateway("besingamkb@gmail.com", "test1q2w");

        // dd($SmsGateway->getMessages());
    	
    	return view('home.home');
    }
}

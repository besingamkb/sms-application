<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Users;
use Session;

class UserController extends Controller
{
	/**
	 * [User Login Page]
	 * @return login page views 
	 */
	public function Login()
	{
		if (Session::get('is_logged_in')) {
    		return redirect('/');
    	}

		$data['title'] = "User Login Page";
		$data['login_page'] = true;

		return view('login.login', $data);
	}

	/**
	 * Authenticate User login
	 * @return json status and result
	 */
	public function Auth(Request $Request)
	{
		$hasRights = Users::where(['email' => $Request->get('email'), 'password' => md5($Request->get('password'))])->get(['id', 'name', 'email', 'level']);
			
		/**
		 * setting session data
		 */
		if (!empty($hasRights->toArray()[0])) {
			Session::put('user_data', $hasRights->toArray()[0]);
			Session::put('is_admin', $hasRights->toArray()[0]['level']);
			Session::put('is_logged_in', true);

			return json_encode(array(
				'status' => 'successful',
				'data' => $hasRights
			));
		}

		return json_encode(array('status' => 'failed'));
	}	

	public function Logout() 
	{
		Session::flush();
		return redirect()->action('HomeController@Index');
	}
}

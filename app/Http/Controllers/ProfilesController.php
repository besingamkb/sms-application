<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Session;
use App\Users;
use App\Http\SmsGateway\SmsGateway;
use App\Profiles;

class ProfilesController extends Controller
{
	private $sms;

	public function __construct()
	{
		$this->sms = new SmsGateway("besingamkb@gmail.com", "test1q2w");


	}

    public function Index($id)
    {
    	$u_id = (Session::get('is_admin') == true) ? $id : Session::get('user_data')['id'];

    	$data = array();
    	$data['users_info'] = Users::find($u_id)->toArray();
    	$data['users_profile'] = Profiles::where('u_id', $u_id)->orderBy('id', 'DESC')->get()->toArray();

    	if (!empty($data['users_profile'][0])) {
    		$data['contact_detail'] = $this->sms->getContact($data['users_profile'][0]['user_id']);	
    	}

    	return view('profiles.edit', $data);
    }

    public function Edit(Request $request)
    {
    	$users = Users::find($request->get('u_id'));

    	if (!empty($request->get('user_id'))) 
    	{
    		$profiles = Profiles::find($request->get('user_id'));
    	} else {
    		$profiles = new Profiles;	
    	}

    	$contactRes = array();
    	
    	$users->name = $request->get('name');
    	$users->email = $request->get('email');
    	if (!empty($request->get('password'))) 
    	{
    		$users->password = $request->get('password');
    	}
    	$users->save();

    	if (!empty($request->get('contact')) || !empty($request->get('position')))
    	{
    		if (!empty($request->get('contact'))) 
    		{
    			$contactRes = $this->sms->createContact($request->get('name'), $request->get('contact'));
    			echo "<pre>"; print_r($contactRes); echo "</pre>";
    			if ($contactRes['response']['success'] == true) 
    			{
    				$users->contact = $contactRes['response']['result']['number'];
    				$profiles->user_id = $contactRes['response']['result']['id'];
    				$profiles->u_id = $request->get('u_id');
    			}
    		} else {
    			$request->u_id = 000000;
    		}
    		if (!empty($request->get('position'))) 
    		{
	    		$profiles->position = $request->get('position');
	    	} else {

	    	}

	   		$profiles->save();
    	}

    	echo "<pre>"; print_r($request->all());
    }
}

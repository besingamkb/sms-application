<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Session;
use App\Users;
use App\Profiles;
use App\Http\SmsGateway\SmsGateway;

class MembersController extends Controller
{
	private $sms;

	public function __construct() 
	{
		$this->sms = new SmsGateway("besingamkb@gmail.com", "test1q2w");
	}

    public function Index() 
    {
    	$data = array();
    	$data['is_admin'] = Session::get('is_admin');
    	
    	$members = Users::all();
    	$data['members'] = $members->toArray();
    	if (Session::get('is_logged_in')) {
    		return view('members.list', $data);
    	}
    }

    public function Add(Request $request)
    {
    	$newmember = new Users;

    	$newmember->name = $request->get('name');
		$newmember->email = $request->get('email');
		$newmember->password = md5($request->get('password'));

		if ($newmember->save())
		{
			return json_encode(array(
				'status' => 'ok',
				'message' => 'new member saved!'
			));
		}
    }

    public function SendMessage($id)
    {
    	$data['device'] = $this->sms->getDevices();

    	$data['profile'] = Profiles::find($id)->orderBy('id', 'DESC')->get()->toArray();
    	$data['details'] = $this->sms->getContact($data['profile'][0]['user_id']);

    	return view('members.send_message', $data);
    }

    public function Send(Request $request) 
    {
    	// $result = $smsGateway->sendMessageToNumber($number, $message, $deviceID, $options);
    	$number = $request->get('number');
    	$message = $request->get('message');
    	$deviceID = $request->get('device_id');

    	$this->sms->sendMessageToNumber($number, $message, $deviceID, array());
    }

    public function Delete($id)
    {
    	$user = Users::find($id);
    	$user->delete();
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
    Route::get('/', 'HomeController@Index');

    Route::get('/user/login/', 'UserController@Login');
    Route::post('/user/auth/', 'UserController@Auth');
    Route::get('/user/logout/', 'UserController@Logout');

    Route::get('/members/', 'MembersController@Index');
    Route::post('/members/add', 'MembersController@Add');
    Route::get('/members/delete/{id}', 'MembersController@Delete');
    Route::get('/members/send_message/{id}', 'MembersController@SendMessage');
    Route::post('/members/send', 'MembersController@Send');

    Route::get('/profiles/{id}', 'ProfilesController@Index');
    Route::post('/profiles/edit', 'ProfilesController@Edit');
});

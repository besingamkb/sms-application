<nav>
  <ul class="nav nav-pills pull-right">
    <li role="presentation"><a href="{{ URL::to('/') }}">Home</a></li>
    <li role="presentation"><a href="{{ URL::to('/members/') }}">Members</a></li>
    <li role="presentation"><a href="{{ URL::to('/profiles/') . "/" . Session::get('user_data')['id'] }}">Edi Profile</a></li>
    <li role="presentation"><a href="{{ URL::to('/user/logout/') }}">Log Out</a></li>
  </ul>
</nav>
<h3 class="text-muted">Sms Application</h3>
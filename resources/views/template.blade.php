<!DOCTYPE html>
<html>
<head>
	<title>
		@if (isset($title))
			{{ $title }}
		@else
			{{ "Sms Application" }}
		@endif
	</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

		@if (isset($login_page) && $login_page == true)
			<link rel="stylesheet" type="text/css" href="http://getbootstrap.com/examples/signin/signin.css">
		@else
			<link rel="stylesheet" type="text/css" href="http://getbootstrap.com/examples/jumbotron-narrow/jumbotron-narrow.css">
		@endif
</head>
<body>
@yield('content')

@include('javascript')
</body>
</html>
@extends('template')

@section('content')
	<div class="container">
      <div class="header clearfix">
		@include('navigation')
        
      </div>
    
	<div class="panel panel-success">
		<div class="panel-heading">EDIT YOUR PROFILE</div>		  
	  	<div class="panel-body">
	    	{{ Form::open(array('action' => 'ProfilesController@Edit', 'method' => 'post', 'id' => 'save_edit_profile')) }}
	    		<div class="alert alert-success" role="alert" style="display: none" id="edit_profile_alert">
		  			Well done! You successfully edit your profile.
		  		</div>
	    		<input type="hidden" name="u_id" value="{{ $users_info['id'] }}">
	    		<input type="hidden" name="user_id" value="{{ !empty($users_profile['user_id']) ? $users_profile['user_id'] : "" }}">
	    		<div class="form-group">
			    	<label for="name">Name</label>
			    	<input type="name" name="name" class="form-control" id="name" value="{{ !empty($users_info['name']) ? $users_info['name'] : "" }}" placeholder="Name">
			  	</div>
			 	<div class="form-group">
			    	<label for="email">Email address</label>
			    	<input type="email" name="email" class="form-control" id="email" value="{{ !empty($users_info['email']) ? $users_info['email'] : "" }}" placeholder="Email">
			  	</div>
				<div class="form-group">
					<label for="password">Password</label>
					<input type="password" name="password" class="form-control" id="password" value="" placeholder="Password">
				</div>
				<div class="form-group">
					<label for="position">Position</label>
					<input type="position" name="position" class="form-control" id="position" value="{{ !empty($users_profile[0]['position']) ? $users_profile[0]['position'] : "" }}" placeholder="Add Position">
				</div>
				<div class="form-group">
					<label for="contact">Contact #</label>
					<input type="contact" name="contact" class="form-control" id="contact" value="{{ !empty($contact_detail['response']['result']['number']) ? $contact_detail['response']['result']['number'] : "" }}" placeholder="Add your contact #">
				</div>
				<input type="submit" class="btn btn-primary" value="Save">
			{{ Form::close() }}
	  	</div>
	</div>


      <footer class="footer">
        <p>&copy; 2016 SmsApp, Inc.</p>
      </footer>

    </div> <!-- /container -->
@stop
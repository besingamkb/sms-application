@extends('template')
@section('content')
	<div class="container">
      <div class="header clearfix">
		@include('navigation')
        
      </div>

      <!-- <div class="jumbotron">
        <h1>Sms Application</h1>
        <p class="lead">Use this site to connect with your team mates.</p>
        <p><a class="btn btn-lg btn-success" href="#" role="button">View Members</a></p>
      </div> -->

      <!-- bootstrap modal -->
      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		    {{ Form::open(array('action' => 'MembersController@Add', 'method' => 'post', 'class' => 'form-signin', 'id' => 'new_member_form')) }}
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="exampleModalLabel">New Member</h4>
		      </div>

		      <div class="modal-body">
		        	<div class="alert alert-success" role="alert" id="new_member_save_alert" style="display: none"> <strong>Well done!</strong> You successfully add new member. </div>
		          <div class="form-group">
		            <label for="name" class="control-label">Name:</label>
		            <input type="text" name="name" class="form-control" id="name" required="">
		          </div>
		          <div class="form-group">
		            <label for="email" class="control-label">Email:</label>
		            <input type="email" name="email" class="form-control" id="email" required="">
		          </div>
		          <div class="form-group">
		            <label for="password" class="control-label">Password:</label>
		            <input type="password" name="password" class="form-control" id="password" required="">
		          </div>
		        
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        <input type="submit" class="btn btn-primary" value="Save">
		      </div>
		      {{ Form::close() }}
		    </div>
		  </div>
		</div>
      <!-- end of bootstrap modal -->
      <div class="alert alert-success" role="alert" id="delete_member_alert" style="display:none"> <strong>Well done!</strong> You successfully delete a member. </div>
      <h3>Member List</h3>
      @if ($is_admin)
      <p>
      	<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">add member</button>
      </p>
      @endif

      <table class="table table-bordered"> 
      	<thead> 
      		<tr> 
      			<th>Name</th> 
      			<th>Email</th> 
      			<th> </th> 
      		</tr> 
      	</thead> 
      	<tbody> 
      		@foreach($members as $member)
      		<tr> 
      			<th>{{ $member['name'] }}</th> 
      			<td>{{ $member['email'] }}</td> 
      			<td>
      				<a href="{{ URL::to('/members/send_message/') . "/" . $member['id'] }}"  class="btn btn-primary btn-xs">
      					<span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> message
      				</a> 
      				@if ($is_admin)
      				<a href="{{ URL::to('/profiles/') . "/" . $member['id'] }}"  class="btn btn-success btn-xs">
      					<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> edit
      				</a> 
      				@elseif ($member['id'] == Session::get('user_data')['id'])
      				<a href="{{ URL::to('/profiles/') . "/" . $member['id'] }}"  class="btn btn-success btn-xs">
      					<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> edit
      				</a> 
      				@endif
      				@if ($is_admin)
      				<a href="" class="delete_member btn btn-danger btn-xs" data-member-id="{{ $member['id'] }}">
      					<span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span> delete
      				</a>
      				@endif
      			</td> 
      		</tr> 
      		@endforeach
      		<!-- <tr> 
      			<th scope="row">233687</th> 
      			<td>Jacob</td> 
      			<td>Thornton</td> 
      		</tr> 
      		<tr> 
      			<th scope="row">238654</th> 
      			<td>Larry</td> 
      			<td>the Bird</td> 
      		</tr>  -->
      	</tbody> 
      </table>

      <footer class="footer">
        <p>&copy; 2016 SmsApp, Inc.</p>
      </footer>

    </div> <!-- /container -->
@stop()
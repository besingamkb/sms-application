@extends('template')
@section('content')
	<div class="container">
      <div class="header clearfix">
		@include('navigation')
        
      </div>


      <div class="panel panel-success">
		<div class="panel-heading">Send Your Message to <strong>{{ $details['response']['result']['name'] }}</strong></div>		  
	  	<div class="panel-body">
	  		<div class="alert alert-success" role="alert" style="display: none" id="send_message_alert">
	  		Well done! You successfully send a message.
	  		</div>
	    	{{ Form::open(array('action' => 'MembersController@Send', 'method' => 'post', 'id' => 'send_message_to_number')) }}
	    		<input type="hidden" name="device_id" value="{{ $device['response']['result']['data'][0]['id'] }}">
	    		<input type="hidden" name="number" value="{{ $details['response']['result']['number'] }}">
	    		<div class="form-group">
			    	<label for="name">Number: {{ $details['response']['result']['number'] }}</label>
			    	<textarea class="form-control" rows="3" name="message" placeholder="message"></textarea>
			  	</div>
				<input type="submit" class="btn btn-primary" value="Send">
			{{ Form::close() }}
	  	</div>
	</div>


      <footer class="footer">
        <p>&copy; 2016 SmsApp, Inc.</p>
      </footer>

    </div> <!-- /container -->
@stop
@extends('template')

@section('content')
		 <!-- <div class="container">

	      <form class="form-signin" method="post" accept="{{ URL::to('/user/auth') }}">
	        <h2 class="form-signin-heading">Please sign in</h2>
	        <label for="inputEmail" class="sr-only">Email address</label>
	        <input type="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
	        <label for="inputPassword" class="sr-only">Password</label>
	        <input type="password" id="inputPassword" class="form-control" placeholder="Password" required>
	        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
	      </form>

	    </div> <!-- /container -->
		    {!! Form::open(array('action' => 'UserController@Auth', 'method' => 'post', 'class' => 'form-signin', 'id' => 'form_login')) !!}
		    	<h2 class="form-signin-heading">Please sign in</h2>
		    	{!! Form::label('inputEmail', 'Email Address', array('class' => 'sr-only')) !!}
		    	{!! Form::email('email', '', array('class' => 'form-control', 'id' => 'inputEmail', 'placeholder' => 'Email address', 'required')) !!}
		    	{!! Form::password('password', array('class' => 'form-control', 'id' => 'inputPassword', 'placeholder' => 'Password', 'required')) !!}
		    	{!! Form::submit('Sign In', array('class' => 'btn btn-lg btn-primary btn-block')) !!}
		    {!! Form::close() !!}

@stop
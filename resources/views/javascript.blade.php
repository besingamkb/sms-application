<!-- Latest compiled and minified JavaScript -->
<script   src="https://code.jquery.com/jquery-1.12.2.min.js"   integrity="sha256-lZFHibXzMHo3GGeehn1hudTAP3Sc0uKXBXAzHX1sjtk="   crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$("#new_member_form").submit(function(event) {
			var data = $(this).serializeArray();

			$.ajax({
				type: 'post',
				url: "{{ URL::to('/members/add') }}",
				data: data,
				success: function (r) {
					$("#new_member_save_alert").show();
					setTimeout(function() {
						location.reload();
					}, 1000);
				}
			});

			event.preventDefault();
		});

		$(".delete_member").on("click", function(event) {
			var mem_id = $(this).data('member-id');
			
			$.ajax({
				url: "{{ URL::to('/members/delete/') }}" + "/" + mem_id,
				success: function(r) {
					console.log(r);
					$("#delete_member_alert").show();
					setTimeout(function() {
						location.reload();
					}, 1000);
				}
			});

			event.preventDefault();
		});

		$("#save_edit_profile").submit(function (event) {
			var data = $(this).serializeArray();
			console.log(data);



			$.ajax({
				type: "post",
				url: $(this).attr('action'),
				data: data,
				success: function(r) {
					$("#edit_profile_alert").show();
					setTimeout(function() {
						location.reload();
					}, 1000);
				}
			});

			event.preventDefault();
		});

		$("#send_message_to_number").submit(function(event) {
			var data = $(this).serializeArray();
			$.ajax({
				type: "post",
				url: $(this).attr('action'),
				data: data,
				success: function(r) {
					$("#send_message_alert").show();
					setTimeout(function() {
						location.reload();
					}, 1000);
				}
			});

			event.preventDefault();
		});

		$('#form_login').submit(function(event) {
			var data = $(this).serializeArray();

			console.log(data);

			$.ajax({
				type: 'post',
				url: $(this).attr('action'),
				data: data,
				dataType: "json",
				success: function (r) {
					if (r.status == "failed") {
						alert("wrong credentials try again");
					} else {
						location.reload();
					}
				}
			});
			event.preventDefault();
		})
	});
</script>
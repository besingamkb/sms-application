<?php

use Illuminate\Database\Seeder;

class SeedAdminAccount extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
        	'name' => 'admin',
        	'email' => 'admin@sms.app',
        	'password' => md5('password'),
        ]);
    }
}
